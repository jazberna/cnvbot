import sys
import re


with open(sys.argv[1],'r') as f:
	for line in f:
		if '#' not in line:
			ret = re.match(r'.*DP4=(.*?);', line)
			dp4 = ret.group(1)
			#print(dp4)
			a,b,c,d=dp4.split(',')
			a = int(a)
			b = int(b)
			c = int(c)
			d = int(d)
			alleles=[a+b,c+d]
			alleles.sort()
			print(alleles[0],"\t",alleles[1])	
