# CNV-bot
<img src="misc/bender.png"  width="60" height="120">

## Motivation:
### Simulate the human intuitive way of believing wether a CNV is true or not by looking at a coverage plot in i.e [IGV](https://software.broadinstitute.org/software/igv/).
<img src="misc/motivation.png"  width="900">


## Preliminaries:

### Download a cram file from 1000 genomes:
    wget ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR323/ERR3239334/NA12878.final.cram

### _Peding to do_: I should now correct the bam for GC content:
- https://deeptools.readthedocs.io/en/2.2.4/content/tools/correctGCBias.html
### generate a tsv file with per base coverage:
This is the example for chr21.

    CRAM=/media/jazamora/disco_grande/CNVCALLER/NA12878.final.cram
    REF=/media/jazamora/disco_grande/references/Homo_sapiens_assembly38.fasta
    CHR=chr21
    samtools depth -a -r ${CHR} --reference  ${REF} ${CRAM} | gzip > coverage.${CHR}.txt.gz

### generate coverage images to train the autoencder
- each image 100 bases (x axis) and max for the y axis is double the average chromosome coverage.
- then each image is resize to 100x100 pixels
- I only produce one image every 100 chunks of 100 bases. Otherwise the number of images produced is massive  
- prints the y axis max value for the training images 
	
```
CRAM=/media/jazamora/disco_grande/CNVCALLER/NA12878.final.cram
REF=/media/jazamora/disco_grande/references/Homo_sapiens_assembly38.fasta
CHR=chr22
WINDOW_SIZE=100
SKIP_BASES=100
samtools mpileup --reference  ${REF} -r ${CHR} ${CRAM}  | awk '{print $1"\t"$2"\t"$4}' > coverage.${CHR}.training.txt
Rscript cnvbot/scripts/generate_training_dataset.R coverage.${CHR}.training.txt $WINDOW_SIZE $SKIP_BASES
tar zcvf ${CHR}_training.tar.gz *png	
```

I have checked that the average covegare is consistent across the autosomes and X by running:

```
CRAM=/home/jazamora/CNVCALLER/NA12878.final.cram
REF=/home/jazamora/references/Homo_sapiens_assembly38.fasta
samtools coverage --reference ${REF} ${CRAM} | awk '{print $1"\t"$7 }' | grep -P '^chr' > chr_coverage.tsv
cat chr_coverage.tsv
```

```
chr1	33.4037
chr2	33.6922
chr3	33.1887
chr4	33.5139
chr5	32.9001
chr6	32.934
chr7	33.2611
chr8	33.0493
chr9	30.166
chr10	34.1433
chr11	33.1778
chr12	33.2689
chr13	29.2459
chr14	27.5399
chr15	27.6595
chr16	34.0959
chr17	34.5613
chr18	32.771
chr19	33.5365
chr20	36.3307
chr21	32.9436
chr22	26.256
chrX	31.7726
chrY	1.87194
chrM	12815.3
```

From those values above I take the maximum coverage (~36 chr20) as the y axis maximum value when the coverage file is looped over and each 100 bases and coverage plot is generated and compared to five artificial references (CN0, CN1, CN2, CN3 and CN>=4). However, those average values are underestimated by regions with no coverage such as centromores. I have got the coords of those regions from the [Canvas Copy Number Variant Caller](https://github.com/Illumina/canvas) and copied them [here](excluded_regions). **Pending:** I have to exclude those regions from the per chr. average coverage calculation, otherwise the the y axis maximum value is underestimated and as a consequence the CN is overestimated.


### this is link to the colab notebook containing the training/testing code:
[autoencoder](https://colab.research.google.com/drive/1_X8pLeEwueQ43kQoAuxTh2PQQCYPG_pe?usp=sharing)

### this is the link to the training images
[chr22_training.tar.gz](dataset/chr22_training.tar.gz)

### this is the link to trained encoder and decoder keras models:
[encoder](models/cnvbot_encoder.h5)

[decoder](models/cnvbot_decoder.h5)

### this is how the 'reference' CN plots and a few real coverage plotslook like for this particular example:
The concept is that each of the real coverage plots is converted into a embedding and is compared to the embedding
of each 'reference' (CN0 CN1 CN2 CN3 CN>=4).

<img src="misc/references.png"  width="300" height="200">

### this is link to the colab notebook that uses the autoencoder to make the predictions:
https://colab.research.google.com/drive/1ceF4smgZ37iti-aPY5Yk7sxaqi2P29k1?usp=sharing

<img src="misc/prediction.png">

### same thing for CN0, CN1, CN3 and CN>=4
<img src="misc/prediction_cn0.png">
<img src="misc/prediction_cn1.png">
<img src="misc/prediction_cn3.png">
<img src="misc/prediction_cn4.png">

After processing the entire chr22 100 base windows the output is [cns.chr22.txt](dataset/cns.chr22.txt) and prediction summary is:

|CN|number_of_windows|
| ---      | ---      |
|2|273904|
|1|66952|
|3|17549|
|4|9363|
|0|140416|

### The output of the CN predictions should have this format (chr, start, end, CN):

    chr22	1	101	0
    chr22	101	201	0
    chr22	201	301	0
    chr22	301	401	0
    ...
    chr22	30013601	30013701	2
    chr22	30013701	30013801	2
    chr22	30013801	30013901	2
    chr22	30013901	30014001	2
    ...
    chr22	50818001	50818101	0
    chr22	50818101	50818201	0
    chr22	50818201	50818301	0
    chr22	50818301	50818401	0

### make segments with this piece of R based on https://kevin-kotze.gitlab.io/tsm/ts-2-tut/:
The make_segments.R takes the output file above [cns.chr22.txt](dataset/cns.chr22.txt) and the maximum number of segments (i.e. 25) 

    Rscript --vanilla cnvbot/scripts/make_segments.R cns.chr22.txt 25 


You will get two files like for chr22 with the segmentation as text and as a plot.

[segments.cns.chr22.txt.tsv](dataset/segments.cns.chr22.txt.tsv)

[segments.cns.chr22.txt.png](dataset/segments.cns.chr22.txt.png)

### this is the segmentation for chr22

    "start end cn"
    "1 10510801 0"
    "10510701 11069001 2"
    "11068901 11284701 1"
    "11284601 11378101 4"
    "11378001 11547401 0"
    "11547301 11631301 4"
    "11631201 12149101 1"
    "12149001 12225601 3"
    "12225501 12275801 0"
    "12275701 12438401 3"
    "12438301 12488901 0"
    "12488801 12641601 3"
    "12641501 12868301 1"
    "12868201 12904801 3"
    "12904701 15154601 0"
    "15154501 15525001 2"
    "15524901 15881701 3"
    "15881601 18239001 2"
    "18238901 18339201 0"
    "18339101 18419701 2"
    "18419601 18483601 0"
    "18483501 18939701 1"
    "18939601 22031501 2"
    "22031401 22698401 1"
    "22698301 22906201 0"
    "22906101 50818401 2"


<img src="dataset/segments.cns.chr22.txt.png"  width="1500">

The black lines are the segmentations.


### _Peding to do_: validate!!
http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/integrated_sv_map/supporting/GRCh38_positions/ALL.wgs.mergedSV.v8.20130502.svs.genotypes.GRCh38.vcf.gz

In the meantime just do some checks back on the cram file correlating the coverage of the segments with the estimated copy number.

    grep -v 'start' segments.cns.chr22.txt.tsv | sed  's/"/ /g' | sort -k4 | awk '{print "samtools coverage -r chr22:" $2"-"$3 " --reference /media/jazamora/disco_grande/references/Homo_sapiens_assembly38.fasta /media/jazamora/disco_grande/CNVCALLER/NA12878.final.cram | grep -v \"#\"    " }' > coverage_commands.tsv
    
    /bin/bash coverage_commands.tsv | awk '{print $7}' > coverages.txt
       
    grep -v 'start' segments.cns.chr22.txt.tsv | sed  's/"/ /g' | sort -k4 | cut -d' ' -f5 > cns.txt

Then in R.

    coverages=read.csv('coverages.txt',header=F)[,1]
    cns=read.csv('cns.txt',header=F)[,1]
    png('chr22_copy_number_segments.png')
    plot(x=as.vector(cns),y=as.vector(coverages),xlab="Copy Number", ylab = "Coverage", main="Copy Number Segments")
    dev.off()

It is clear from the plot an evident correlation between the coverage and the estimnated copy number of the segments.

<img src="misc/chr22_copy_number_segments.png"  width="400">

Finally the IGV screenshots for the last break points

<img src="misc/igv.png"  width="700">

I also validated the CN3 segment chr22:12149001-12225601 by calling SNVs in that region and calulating the MAF of every variant which shuould be, and it is , around 0.33.

    BAM=/media/jazamora/disco_grande/CNVCALLER/NA12878.final.bam
    REF=/media/jazamora/disco_grande/references/Homo_sapiens_assembly38.fasta
    /home/jazamora/SOFTWARE/bcftools/bcftools mpileup --regions chr22:12149001-12225601 -f $REF $BAM | /home/jazamora/SOFTWARE/bcftools/bcftools call -mv -Ov -o calls.vcf
    python  scripts/get_af.py calls.vcf > allele_counts.tsv
    
    # then in R
    MAFs=c()
    count_data = read.csv('allele_counts.tsv',sep='\t', header=F)
    for (i in c(1:nrow(count_data))){
        counts = count_data[i,]
        MAF=as.numeric(counts[1]/sum(counts))
        MAFs = c(MAFs,MAF)
    }
    mean(MAFS)
    0.3180038
    hist(MAFs,breaks=100,xlim=c(0,1))

<img src="misc/cn3.png"  width="400">

### _Peding to do_: make it a somatic caller?

### _Peding to do_: Investivate if these patterns are useful to create extra references.
<img src="misc/pattern1.png"  width="700">
<img src="misc/pattern2.png"  width="700">

